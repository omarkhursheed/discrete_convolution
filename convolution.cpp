#include <iostream>
using namespace std;
int main(int argc, char const *argv[])
{
    int n1,n2,i,k, i1 = 0, i2 = 0;
    int x[10000];
    int h[10000];
    int y[10000] = {0};
    cout << "Welcome to convolution:" << endl;
    cout << "How many entries would you like to enter for x[n]?" << endl;
    cin >> n1;
    cout << "\nWhat index do you want your first entry at?" << endl;
    cin >> i1;
    cout << "Enter the values for x[n]" << endl;
    for (i = 0; i < n1; i++)
        cin >> x[i];
    cout << endl << "How many entries would you like to enter for h[n]" << endl;
    cin >> n2;
    cout << "\nWhat index do you want your first entry at?" << endl;
    cin >> i2;
    cout << "Enter values for h[n]" << endl;
    for (i = 0; i < n2; i++)
        cin >> h[i];
    int l = 0;
    for (i = 0; i < n2; i++)
    {
        for (k = 0; k < n1; k++)
            {
                y[l] += x[k]*h[i];
                l++;
            }
        l = i + 1;
    }
    cout << "The result of the convolution is: " << endl;
    int index = i1 + i2;
    cout << "Index\tValue\n";
    for (k = 0; k < (n1 + n2 -1); k++)
    {
        cout << index << "\t" << y[k] << endl;
        index++;
    }
    return 0;
}